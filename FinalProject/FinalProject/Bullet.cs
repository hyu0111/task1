﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    // the bullet shoot by the aircraft
    public class Bullet : DrawableGameComponent
    {
        // parent game
        Game1 parent;
        // bullet shooting speed; y-axis speed should be 0
        private Vector2 speed = new Vector2(30, 0);
        // the bullet Texture2D image
        private Texture2D bullet;
        // where the bullet start to shoot
        private Vector2 startPosition;

        public Bullet(Game game, Vector2 position) : base(game)
        {
            parent = (Game1) game;
            // load the bullet 2d image
            bullet = parent.Content.Load<Texture2D>("bullet/bullet");
            // set start position based on where the plane currently at
            startPosition = position;
        }

        public override void Draw(GameTime gameTime)
        {
            // draw the new bullet if it is visible
            if (this.Visible)
            {
                parent.Sprite.Begin();
                // draw the bullet
                parent.Sprite.Draw(bullet, startPosition, Color.White);
                parent.Sprite.End();
            }
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            // update only if it is on the screen and is enabled
            if ((this.Enabled) && (parent.Stage.X >= startPosition.X))
            {
                // update the new position to 30 units to the right
                startPosition.X += speed.X;
            }
            base.Update(gameTime);
        }

        public bool OutOfScreen()
        {
            // checks if bullet travels out of the screen
            return parent.Stage.X <= startPosition.X;
        }

        public bool IsHit()
        {
            // the bullet is hit and no longer valid (to hit another enemy)
            return (!this.Visible && !this.Enabled);
        }

        public void Hit()
        {
            // if a bullet is hit, then we no longer need it; set to not visible and disable it
            this.Visible = false;
            this.Enabled = false;
        }

        public Vector2 getHitPoint()
        {
            // get the hit point, which is the middle right point of the bullet
            return new Vector2(startPosition.X + bullet.Width, startPosition.Y + bullet.Height / 2);

        }
    }
}
