﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    class LevelScene : GameScene
    {
        // font to display level texts
        private SpriteFont levelFont;
        // position to display the level texts
        private Vector2 positionLevelText;
        // color of level texts
        private Color levelColor = Color.White;
        // color of the level selected
        private Color selectedLevelColor = Color.Yellow;

        // text items for levels
        private List<string> difficultyItems = new List<String>()
        {
            "Easy",
            "Hard"
        };
        // indicate which level is selected
        private int selectedIndex = 0;

        // texts to help user select...
        private string descriptionText = "Please select difficulty to start";
        private string returnText = "Press ESC to return to menu";
        // position to display the helper texts
        private Vector2 positionHelpText;

        // previous state of keyboard
        private KeyboardState oldState;

        // check if enter is hit for the first time
        // there's an issue that if I hit enter in menu scene, it automatically skips the level scene and starts the game
        // this value is used to prevent entering the game immediately
        bool isEnterHit = false;

        public LevelScene(Game game, SpriteFont levelFont) : base(game)
        {
            // positions of help text and level text
            positionLevelText = new Vector2(parent.Stage.X / 2 - 10, parent.Stage.Y / 2);
            positionHelpText = new Vector2(parent.Stage.X / 5, parent.Stage.Y / 2);
            this.levelFont = levelFont;
        }

        public override void Draw(GameTime gameTime)
        {
            // draw background 
            parent.DrawGeneralBackground();
            // draw title
            parent.DrawTitle();

            // start drawing
            parent.Sprite.Begin();
            // temp position
            Vector2 tempPosition = positionHelpText;

            // draw first help text
            parent.Sprite.DrawString(levelFont, descriptionText, tempPosition, levelColor);
            tempPosition.Y += levelFont.LineSpacing * 2;
            tempPosition.X = positionLevelText.X;

            // draw each level items
            for (int i = 0; i < difficultyItems.Count; i++)
            {
                // if option is selected then display with different color
                if (selectedIndex == i)
                {
                    parent.Sprite.DrawString(levelFont, difficultyItems[i], tempPosition, selectedLevelColor);
                }
                else
                {
                    parent.Sprite.DrawString(levelFont, difficultyItems[i], tempPosition, levelColor);
                }
                tempPosition.Y += levelFont.LineSpacing;
            }
            // update the position to draw the last help line
            tempPosition.X = positionHelpText.X;
            tempPosition.Y += levelFont.LineSpacing;
            // draw the last help text
            parent.Sprite.DrawString(levelFont, returnText, tempPosition, levelColor);


            // end drawing
            parent.Sprite.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState state = Keyboard.GetState();
            // move one position down for level selection
            if (oldState.IsKeyUp(Keys.Down) && state.IsKeyDown(Keys.Down))
            {
                selectedIndex = MathHelper.Clamp(selectedIndex + 1, 0, difficultyItems.Count - 1);
            }
            // move one position up for level selection
            if (oldState.IsKeyUp(Keys.Up) && state.IsKeyDown(Keys.Up))
            {
                selectedIndex = MathHelper.Clamp(selectedIndex - 1, 0, difficultyItems.Count - 1);
            }
            // enter is hit. Entering action scene
            if (oldState.IsKeyUp(Keys.Enter) && state.IsKeyDown(Keys.Enter))
            {
                // update scene or is enter hit value if the page is visible and enabled
                if (this.Enabled && this.Visible)
                {
                    // when enter is hit for the first time
                    // prevent from automatically choose easy level and start
                    if (!isEnterHit)
                    {
                        isEnterHit = true;
                    }
                    else
                    {
                        // hit the second time, switch to the action scene
                        parent.Notify(this, difficultyItems[selectedIndex]);
                        // isEnterHit = false;
                    }
                }
                
            }
            // user press escape, so go back to main menu
            if (oldState.IsKeyUp(Keys.Escape) && state.IsKeyDown(Keys.Escape))
            {
                parent.Notify(this, "Escape");
            }

            oldState = state;

            base.Update(gameTime);
        }
    }
}
