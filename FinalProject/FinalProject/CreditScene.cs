﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    class CreditScene : GameScene
    {
        // previous state of keyboard
        private KeyboardState oldState;

        // position to place the credit texts
        private Vector2 positionCreditText;

        // texts to display at credit page
        private List<string> creditItems = new List<string>(){
            "Created By: Haokun Yu",
            "Images and soundtracks from Internet"
        };
        // help text for return to menu
        private string returnToMain = "Press ESC to return to main menu";

        // font to display texts
        private SpriteFont creditFont;
        // color to display texts
        private Color creditColor = Color.White;

        public CreditScene(Game game, SpriteFont creditFont) : base(game)
        {
            // set position to display text
            positionCreditText = new Vector2(parent.Stage.X / 5, parent.Stage.Y / 2);
            this.creditFont = creditFont;
        }

        public override void Draw(GameTime gameTime)
        {
            // draw background 
            parent.DrawGeneralBackground();
            // draw title
            parent.DrawTitle();

            // start drawing
            parent.Sprite.Begin();
            // temp position of menu item
            Vector2 tempPosition = positionCreditText;

            // draw each credit text items
            for (int i = 0; i < creditItems.Count; i++)
            {
                parent.Sprite.DrawString(creditFont, creditItems[i], tempPosition, creditColor);
                // add new line
                tempPosition.Y += creditFont.LineSpacing;
            }

            tempPosition.Y += creditFont.LineSpacing;
            // draw the line to tell press ESC to return to home
            parent.Sprite.DrawString(creditFont, returnToMain, tempPosition, creditColor);


            // end drawing
            parent.Sprite.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            // current keyboard state
            KeyboardState state = Keyboard.GetState();
            
            // if ESC is pressed
            if (oldState.IsKeyUp(Keys.Escape) && state.IsKeyDown(Keys.Escape))
            {
                // notify main game and return to menu page
                parent.Notify(this, "escape");
            }
            oldState = state;
            base.Update(gameTime);
        }
    }
}
