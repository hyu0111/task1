﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    class EnemyManager : DrawableGameComponent
    {
        // indicate if the current level is difficult
        bool isDifficult;
        Game1 parent;

        // image resources of each different enemy
        Texture2D[] enemy1;
        Texture2D[] enemy2;
        Texture2D[] enemy3;

        // travel speed of each enemy
        // change enemy speed to slow down the enemies
        Vector2 enemy1speed = new Vector2(8, 0);
        Vector2 enemy2speed = new Vector2(12, 0);
        Vector2 enemy3speed = new Vector2(16, 0);

        // list of current alive enemies
        List<Enemy> enemies;

        // a clock to check when to generate a new enemy
        int clock = 0;

        // action scene where the enemies are shown and plane live count are stores
        ActionScene actionScene;

        public EnemyManager(Game game, bool isDifficult, GameScene actionScene) : base(game)
        {
            parent = (Game1)game;
            // indicates if the game is difficult or easy
            this.isDifficult = isDifficult;
            // new list to store all current alive enemiesi
            enemies = new List<Enemy>();

            // resources of all enemy types
            enemy1 = new Texture2D[1] {
                parent.Content.Load<Texture2D>("enemy1/enemy1")
            };
            enemy2 = new Texture2D[2] {
                parent.Content.Load<Texture2D>("enemy2/enemy2"),
                parent.Content.Load<Texture2D>("enemy2/enemy2_explode1")
            };
            enemy3 = new Texture2D[3] {
                parent.Content.Load<Texture2D>("enemy3/enemy3"),
                parent.Content.Load<Texture2D>("enemy3/enemy3_explode1"),
                parent.Content.Load<Texture2D>("enemy3/enemy3_explode2")
            };
            // action scene
            this.actionScene = (ActionScene)actionScene;
        }

        public override void Draw(GameTime gameTime)
        {
            foreach(Enemy e in enemies)
            {
                if (e.isAlive())
                {
                    // draw the enemy in new position if it is still alive
                    e.Draw(gameTime);
                }
            }
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            // check each enemy and see if they are still alive, being hit, or travels to the leftmost point already
            for (int i = enemies.Count - 1; i >= 0; --i)
            {
                // remove the enemy if it isi not alive (being shoot by the aircraft)
                if (!enemies[i].isAlive())
                {
                    enemies.RemoveAt(i);
                }
                // if the enemy travels out of the screen (yet still alive at that point)
                else if (enemies[i].OutOfScreen())
                {
                    enemies.RemoveAt(i);
                    // this case aircraft player love one live
                    actionScene.LoseOneLive();
                }
                else
                {
                    // update the enemy's new position
                    enemies[i].Update(gameTime);
                }
            }
            // add counter by 1
            clock += 1;

            // if the player clears the board, we immediately generate a new enemy
            if(enemies.Count < 1)
            {
                clock = 1000;
            }
            Enemy newEnemy = null;

            // if it is difficult and we wait for enough time
            // change clock count limit to slow down the generation of enemy
            if (isDifficult && clock >= 30)
            {
                // reset counter
                clock = 0;
                // randomly generate a new enemy type
                int nextEnemy = new Random().Next(0, 3);
                // create new enemy based on the random generated enemy type
                if (nextEnemy == 0)
                {
                    newEnemy = new Enemy(parent, nextEnemy, enemy1, enemy1speed);
                }
                else if (nextEnemy == 1)
                {
                    newEnemy = new Enemy(parent, nextEnemy, enemy2, enemy2speed);
                }
                else
                {
                    newEnemy = new Enemy(parent, nextEnemy, enemy3, enemy3speed);
                }

            }
            // if it is easy level and we wait for enought time
            else if (!isDifficult && clock >= 40)
            {
                // reset clock
                clock = 0;
                // randomly generate a new enemy type
                int nextEnemy = new Random().Next(0, 2);
                // create new enemy based on the random generated enemy type
                if (nextEnemy == 0)
                {
                    newEnemy = new Enemy(parent, nextEnemy, enemy1, enemy1speed);
                }
                else
                {
                    newEnemy = new Enemy(parent, nextEnemy, enemy2, enemy2speed);
                }
            }

            if (newEnemy != null)
            {
                // add the enemy if it generated successfully
                enemies.Add(newEnemy);
            }

            base.Update(gameTime);
        }

        public List<Enemy> GetEnemies()
        {
            // get all currently available enemies
            return enemies;
        }
    }
}
