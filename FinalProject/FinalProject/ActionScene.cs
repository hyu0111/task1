﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;


using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace FinalProject
{
    class ActionScene : GameScene
    {
        // indicate if the game is running, or the game ends already
        private bool isPlaying = false;
        // indicate if the user pauses the game (the game can be resumed)
        private bool paused = false;
        // indicate if the user selects easy or difficult game level
        private bool difficultySwitch = false; // true: difficult; false: easy

        // font to display live, score, and other messages
        private SpriteFont font;
        // color for information text
        private Color infotextColor = Color.White;

        // previous state of keyboard
        private KeyboardState oldState;

        // scrolling background
        private GameScrollingBackground background;
        // aircraft
        private Plane plane;

        // live counter items
        // how many lives the player still have
        private int liveCount;
        // text to display lives
        private string liveText = "Lives remaining: ";
        // location to start displaying current lives
        private Vector2 liveCountPosition = new Vector2(0, 0);

        // text to display scores
        private string scoreText = "Score: ";
        // count how many enemies the player shoot (score)
        private int scoreCount;
        
        // items to show pause
        // pause text
        private string pauseText = "PAUSED";
        // font to show pause
        private SpriteFont pauseFont;
        // location to show pause text
        private Vector2 middleTitlePosition;
        // pause text color
        private Color pauseColor = Color.LightYellow;

        // help texts show at game pause
        private string escText = "Press ESC to go to menu";
        private string continueText = "Press ENTER to continue the game";

        // info text to show at game end
        private string gameEndText = "GAME END";
        private string highScoreText = "Highest Score: ";

        // manager that handles enemy aircrafts
        private EnemyManager enemyManager;

        // all the explosions...
        private List<Explosion> explosions = new List<Explosion>();

        // sound effect to play at game end
        private SoundEffect endSound;

        public ActionScene(Game game, SpriteFont font, SpriteFont pauseFont) : base(game)
        {
            // set fonts
            this.font = font;
            this.pauseFont = pauseFont;
            // set location of pause text
            middleTitlePosition = new Vector2(parent.Stage.X / 4, parent.Stage.Y / 3);
            // set background
            background = new GameScrollingBackground(parent);
            // use restart to set data, but set the game to not playing
            Restart();
            isPlaying = false;
            // set game end sound
            endSound = parent.Content.Load<SoundEffect>("sound/endGame");
        }

        public void SetLevel(bool difficulty)
        {
            if (!isPlaying)
            {
                // update level only if the game is not playing
                difficultySwitch = difficulty;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            // if the game is not paused and not ended (still playing)
            // then we update the background, aircrafts, bullets, and explosions
            if (!paused && isPlaying)
            {
                GraphicsDevice.Clear(Color.Black);
                // background first because it's at the back
                background.Draw(gameTime);
                plane.Draw(gameTime);
                enemyManager.Draw(gameTime);
                foreach(Explosion e in explosions)
                {
                    e.Draw(gameTime);
                }
            }
            else
            {
                // the game is paused or ended, then we use another background and not update
                // the aircrafts/bullets/explosions...
                parent.DrawGeneralBackground();
            }

            parent.Sprite.Begin();
            if (paused && isPlaying)
            {
                // the game is paused, but not ended
                // temporary position to update
                Vector2 tempPosition = middleTitlePosition;
                // draw the pause text
                parent.Sprite.DrawString(pauseFont, pauseText, tempPosition, pauseColor);
                // update to write next line at new location
                tempPosition.Y += pauseFont.LineSpacing + font.LineSpacing;
                tempPosition.X = parent.Stage.X / 5;
                // write the help texts
                parent.Sprite.DrawString(font, escText, tempPosition, infotextColor);
                tempPosition.Y += font.LineSpacing;
                parent.Sprite.DrawString(font, continueText, tempPosition, infotextColor);
            }

            if (isPlaying)
            {
                // display score and lives whenever the game is not yet ended (can pause)
                Vector2 tempPosition = liveCountPosition;
                parent.Sprite.DrawString(font, liveText + liveCount, tempPosition, infotextColor);
                tempPosition.Y += font.LineSpacing;
                parent.Sprite.DrawString(font, scoreText + scoreCount, tempPosition, infotextColor);
            }
            else
            {
                // game ended already
                // read scores
                List<int> scores = ScoreScene.ReadScores(parent.GetLeaderBoardFilename(difficultySwitch));
                // temp position to update
                Vector2 tempPosition = middleTitlePosition;
                // draw the game ended text
                parent.Sprite.DrawString(pauseFont, gameEndText, tempPosition, pauseColor);
                // update to write next line at new location
                tempPosition.Y += pauseFont.LineSpacing;
                tempPosition.X = parent.Stage.X / 3;
                // draw user score
                parent.Sprite.DrawString(font, scoreText + scoreCount, tempPosition, infotextColor);
                tempPosition.Y += font.LineSpacing;
                // draw highest score
                if (scores.Contains(-1) || scores.Count == 0)
                {
                    // if there is no score in record, or there's error reading score
                    parent.Sprite.DrawString(font, highScoreText + MathHelper.Max(0, scoreCount), tempPosition, infotextColor);
                }
                else
                {
                    // display highest score, the higher of current game score or the one in record
                    parent.Sprite.DrawString(font, highScoreText + MathHelper.Max(scores[0], scoreCount), tempPosition, infotextColor);
                }

            }
            parent.Sprite.End();
            base.Draw(gameTime);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        // minus live by 1
        public void LoseOneLive()
        {
            liveCount -= 1;
        }

        // restart the game
        public void Restart()
        {
            // create new plane
            plane = new Plane(parent);
            // create new enemy manager
            enemyManager = new EnemyManager(parent, difficultySwitch, this);
            // set lives based on the difficulty
            if (difficultySwitch)
            {
                liveCount = 10;
            }
            else
            {
                liveCount = 5;
            }
            // reset score
            scoreCount = 0;
            // reset the explosion list
            explosions = new List<Explosion>();
            // start game
            isPlaying = true;
        }

        public override void Update(GameTime gameTime)
        {
            // update background
            background.Update(gameTime);

            // check if player still have live
            if (liveCount == 0)
            {
                // stop the game once the game ended
                isPlaying = false;
                // set to -1 so we only access here once
                // we only want to update the score and play the sound once
                liveCount = -1; 
                // update score to file
                ScoreScene.UpdateScores(parent.GetLeaderBoardFilename(difficultySwitch), scoreCount);
                // play end game sound
                endSound.Play();
            }

            // get keyboard state
            KeyboardState state = Keyboard.GetState();

            // if the game ended already, only want to detect escape (return to menu) and enter (restart)
            if (!isPlaying)
            {
                if (oldState.IsKeyUp(Keys.Escape) && state.IsKeyDown(Keys.Escape))
                {
                    // go back to main menu, notify
                    parent.Notify(this, "Escape");
                }
                if (oldState.IsKeyUp(Keys.Enter) && state.IsKeyDown(Keys.Enter))
                {
                    // reset all data and set game to playing mode
                    Restart();
                }
            }
            // if the game is only paused but still in game
            else if (paused)
            {
                if (oldState.IsKeyUp(Keys.Escape) && state.IsKeyDown(Keys.Escape))
                {
                    // go back to the main menu, stop the game and notify
                    isPlaying = false;
                    // paused = false;
                    parent.Notify(this, "Escape");
                }
                if (oldState.IsKeyUp(Keys.Enter) && state.IsKeyDown(Keys.Enter))
                {
                    // continue with the game
                    paused = false;
                }

            }
            // the game is playing and not paused
            else
            {
                // check each bullet and plane and see if they collide
                foreach (Bullet b in plane.GetBullets())
                {
                    // if the bullet is travelled out of the screen (so cannot hit an enemy), or it already hits an enemy
                    if (b.OutOfScreen() || b.IsHit())
                    {
                        continue;
                    }
                    foreach (Enemy e in enemyManager.GetEnemies())
                    {
                        // if the enemy aircraft is out of live already
                        if (!e.isAlive())
                        {
                            continue;
                        }
                        // both bullet and enemy are valid, then check collision
                        if (DetectCollision(e, b))
                        {
                            // add one score
                            scoreCount += 1;
                            // add explosion animation
                            explosions.Add(new Explosion(parent, b.getHitPoint()));
                            // set both bullet and enemy to hit state
                            b.Hit();
                            e.Hit();
                            // break here because the bullet (and enemy) is no longer valid
                            break;
                        }
                    }
                }

                // update the plane. plane controls are done inside the plane
                plane.Update(gameTime);
                // update the enemy aircrafts
                enemyManager.Update(gameTime);
                // update animation of explosion
                for (int i = explosions.Count - 1; i >= 0; i--)
                {
                    // remove the explosion from list once it's done
                    if (explosions[i].AnimationDone())
                    {
                        explosions.RemoveAt(i);
                    }
                    else
                    {
                        // otherwise update animation
                        explosions[i].Update(gameTime);
                    }
                }
                // pause the game if ESC is pressed
                if (oldState.IsKeyUp(Keys.Escape) && state.IsKeyDown(Keys.Escape))
                {
                    paused = true;

                }
            }
            // update old state
            oldState = state;
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            // add background
            this.Components.Add(background);
            // add aircraft
            this.Components.Add(plane);
            base.LoadContent();
        }

        public override void show()
        {
            if (!isPlaying)
            {
                // reset the scene if it is a new game cycle
                Restart();
            }
            base.show();
        }

        // check if the bullet collide with enemy aircraft
        private bool DetectCollision(Enemy enemy, Bullet bullet)
        {
            bool collide = false;
            // get the bullet head point
            Vector2 bulletHitPoint = bullet.getHitPoint();
            // get the enemy hit area
            Rectangle enemyHitArea = enemy.GetHitArea();
            // check if bullet inside the enemy hit area
            if (enemyHitArea.Contains(bulletHitPoint))
            {
                collide = true;
            }

            return collide;
        }
    }

}
