﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    class ScoreScene : GameScene
    {
        // previous state of keyboard
        private KeyboardState oldState;

        // position to display 
        private Vector2 positionScoreText;

        // lists to store easy mode and difficult/hard mode scores
        private List<int> scoresEasy;
        private List<int> scoresHard;

        // font to display score items
        private SpriteFont scoreFont;
        // color of score texts
        private Color scoreColor = Color.White;

        // displays when there's no game score data in the file
        private string noscoreText = "No data";
        // displays when there's error reading the file
        private string readFileErrorText = "Error reading file. ";
        // top 5 text, use above the scores
        private string top5Text = "Top 5 ";
        // text indicate the ranking of player
        private string[] rankText = { "First: ", "Second: ", "Third: " , "Fourth: ", "Fifth: "};
        // help texts to return to menu
        private string returnToMain = "Press ESC to return to main menu";


        public ScoreScene(Game game, SpriteFont scoreFont) : base(game)
        {
            // upperleft position to show the scores
            positionScoreText = new Vector2(parent.Stage.X / 9, parent.Stage.Y / 2);
            this.scoreFont = scoreFont;
        }
        public override void Draw(GameTime gameTime)
        {
            // draw background 
            parent.DrawGeneralBackground();
            // draw title
            parent.DrawTitle();

            // start drawing
            parent.Sprite.Begin();

            // temp position to start draw scores
            Vector2 tempPosition = positionScoreText;

            // draw each menu items
            for (int d = 0; d < 2; d++)
            {
                // reset temp position of menu item
                tempPosition = positionScoreText;
                // list of scores
                List<int> scores;
                if (d == 0)
                {
                    // update the score list locally with easy list
                    scores = scoresEasy;
                    // draw title as easy in easy mode
                    parent.Sprite.DrawString(scoreFont, top5Text + "(Easy)", tempPosition, scoreColor);
                }
                else
                {
                    // update the list locally with hard list
                    scores = scoresHard;
                    // shift the temp display position to the right for hard mode
                    tempPosition.X = parent.Stage.X / 2;
                    // draw title as hard in hard mode
                    parent.Sprite.DrawString(scoreFont, top5Text + "(Hard)", tempPosition, scoreColor);
                }
                // add spacing
                tempPosition.Y += scoreFont.LineSpacing;

                // check if there's data in the file
                if (scores.Count == 0)
                {
                    // no data in the file, display no score text
                    parent.Sprite.DrawString(scoreFont, noscoreText, tempPosition, scoreColor);
                }
                // list is not empty
                else if (scores.Contains(-1))
                {
                    // if it contains -1, that means there's error reading file. Score can never be negative
                    parent.Sprite.DrawString(scoreFont, readFileErrorText, tempPosition, scoreColor);
                }
                else
                {
                    int count = 0;
                    foreach (int s in scores)
                    {
                        // draw top 5 scores to the screen
                        // if it is less than 5, it leaves the for loop not showing the not existing scores
                        parent.Sprite.DrawString(scoreFont, rankText[count] + s, tempPosition, scoreColor);
                        // add spacing
                        tempPosition.Y += scoreFont.LineSpacing;
                        count += 1;
                        if (count == 5)
                        {
                            // break when we have displayed 5 players
                            break;
                        }
                    }
                }
            }
            // update the temp position.
            // There are at most 5 player scores displayed, counting with title line and extra spacing, we add 7 line spacings for back to menu text
            tempPosition.Y = positionScoreText.Y + scoreFont.LineSpacing * 7;
            // reset x axis as well because hard mode shifts the x axis
            tempPosition.X = positionScoreText.X;
            // draw back to menu text
            parent.Sprite.DrawString(scoreFont, returnToMain, tempPosition, scoreColor);

            // end drawing
            parent.Sprite.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            // current keyboard state
            KeyboardState state = Keyboard.GetState();
            // ESC is pressed so go back to menu
            if (oldState.IsKeyUp(Keys.Escape) && state.IsKeyDown(Keys.Escape))
            {
                parent.Notify(this, "escape");
            }
            oldState = state;
            base.Update(gameTime);
        }

        public override void show()
        {
            // read the score when the page is shown
            // only need to read once at each show because it won't change until user leaves the score page and play a new game
            scoresEasy = ReadScores(parent.GetLeaderBoardFilename(false));
            scoresHard = ReadScores(parent.GetLeaderBoardFilename(true));
            base.show();
        }

        // read scores based from the file name given (can be easy or hard, the method doesn't know)
        public static List<int> ReadScores(string filename)
        {
            // list to store the ints read from file
            List<int> values = new List<int>();

            try
            {
                // if the file doesn't exist, create the file first
                if (!File.Exists(filename))
                {
                    using (StreamWriter file = new StreamWriter(filename)) { }
                }
                // read all data from the file and conver them to integers
                using (StreamReader file = new StreamReader(filename))
                {
                    string line;
                    // read the rest of the file and process it after
                    while ((line = file.ReadLine()) != null)
                    {
                        values.Add(Convert.ToInt32(line));
                    }
                    file.Close();
                }
            }
            catch (Exception)
            {
                // failed to read or convert to integer, so we add -1 to list to tell user that there's error reading the file
                values.Add(-1);
            }
            // sort the scores from small to large
            values.Sort();
            // reverse the scores so the first one is largest
            values.Reverse();
            return values;
        }

        public static void UpdateScores(string filename, int newScore)
        {
            // get all scores currently in record
            List<int> values = ScoreScene.ReadScores(filename);
            // error reading the file, return and not write the new score
            if (values.Contains(-1))
            {
                return;
            }
            // add the new score to the values
            values.Add(newScore);
            // sort the scores from small to large
            values.Sort();
            // reverse the scores so the first one is largest
            values.Reverse();
            try
            {
                using (StreamWriter file = new StreamWriter(filename))
                {
                    // how many scores is written to file
                    int count = 0;
                    foreach(int i in values)
                    {
                        // write the current read score no matter what
                        file.WriteLine(i);
                        count += 1;
                        // if we have written more than 5 scores, break
                        // we only need to store at least 5 top scores
                        if (count == 5)
                        {
                            break;
                        }
                    }
                    file.Close();
                }
            }
            // do nothing if there's error writing file
            catch { }
        }
    }
}
