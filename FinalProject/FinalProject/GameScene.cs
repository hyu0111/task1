﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    // game scene for all scenes
    public class GameScene : DrawableGameComponent
    {

        protected Game1 parent;
        // components in the scene
        public List<GameComponent> Components { get; set; }


        public GameScene(Game game) : base(game)
        {
            parent = (Game1)game;
            Components = new List<GameComponent>();
            hide();
        }

        // set current scene state, either show the scene or hide
        public virtual void SetState(bool state)
        {
            // set scene state
            this.Enabled = state;
            this.Visible = state;
            foreach (GameComponent item in Components)
            {
                item.Enabled = state;
                if (item is DrawableGameComponent)
                {
                    // set the state of component if it is drawable game component
                    DrawableGameComponent comp = (DrawableGameComponent)item;
                    comp.Visible = state;
                }
            }
        }

        // hide the game scene
        public virtual void hide()
        {
            SetState(false);
        }
        // show the game scene
        public virtual void show()
        {
            SetState(true);
        }

        public override void Update(GameTime gameTime)
        {
            foreach (GameComponent item in Components)
            {
                // update each component if it is enabled
                if (item.Enabled)
                {
                    item.Update(gameTime);
                }
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawableGameComponent comp = null;
            foreach(GameComponent item in Components)
            {
                if (item is DrawableGameComponent)
                {
                    // draw each component if it is drawable game component and is visible
                    comp = (DrawableGameComponent)item;
                    if (comp.Visible)
                    {
                        comp.Draw(gameTime);
                    }
                }
            }
            base.Draw(gameTime);
        }
    }
}
