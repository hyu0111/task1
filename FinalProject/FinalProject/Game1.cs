﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace FinalProject
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public SpriteBatch Sprite { get => spriteBatch; }
        public Vector2 Stage { get; set; }

        // size of screen
        public int GAME_WIDTH { get => 1280; }
        public int GAME_HEIGHT { get => 800; }

        // background (general, not in game one)
        private Texture2D background;

        // font for general information display
        private SpriteFont generalFont;

        // the current scene
        private GameScene currentScene;

        // menu scene
        private MenuScene menuScene;
        // menu items
        private List<string> menuItems = new List<string>()
        {
            "Start Game",
            "Help",
            "High Score",
            "About", // credit
            "Quit"
        };

        // action scene
        private ActionScene actionScene;

        // help scene
        private HelpScene helpScene;

        // score scene
        private ScoreScene scoreScene;

        // file name that stores the top scores
        private string leaderboardFilenameEasy = "leaderboard.txt";
        private string leaderboardFilenameHard = "leaderboardHard.txt";

        // credit/about scene
        private CreditScene creditScene;

        // level scene
        private LevelScene levelScene;

        // title items
        // title font
        private SpriteFont titleFont;
        // title front color
        private Color titleColor = Color.Yellow;
        // title back color
        private Color titleColorBakground = Color.OrangeRed;
        // title text
        private string titleText = "RAIDEN";
        // title position
        private Vector2 positionTitle;

        // music play at the back
        Song backgroundMusic;


        // generated constructor
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {

            // set stage vector object
            Stage = new Vector2(GAME_WIDTH, GAME_HEIGHT);

            // set windows width ahdn height
            graphics.PreferredBackBufferWidth = GAME_WIDTH;  
            graphics.PreferredBackBufferHeight = GAME_HEIGHT;

            // set title position
            positionTitle = new Vector2(Stage.X / 3, Stage.Y / 4);
            // load title font
            titleFont = Content.Load<SpriteFont>("Fonts/Title");
            // load menu font
            generalFont = Content.Load<SpriteFont>("Fonts/Other");
            // load background
            background = Content.Load<Texture2D>("background/background");

            // load background music
            // backgroundMusic = Content.Load<SoundEffect>("sound/bgm");
            backgroundMusic = Content.Load<Song>("sound/bgm");
            //MediaPlayer.IsRepeating = true;
            //MediaPlayer.Play(backgroundMusic);

            base.Initialize();
            graphics.ApplyChanges();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // load menu scene
            menuScene = new MenuScene(this, menuItems, generalFont);
            this.Components.Add(menuScene);
            currentScene = menuScene;
            // show menu scene
            currentScene.show();

            // create ACTOIN scenes
            actionScene = new ActionScene(this, generalFont, titleFont);
            this.Components.Add(actionScene);

            // create CREDIT scenes
            creditScene = new CreditScene(this, generalFont);
            this.Components.Add(creditScene);

            // create SCORE scenes
            scoreScene = new ScoreScene(this, generalFont);
            this.Components.Add(scoreScene);

            // create HELP scenes
            helpScene = new HelpScene(this, generalFont);
            this.Components.Add(helpScene);

            // create LEVEL scene
            levelScene = new LevelScene(this, generalFont);
            this.Components.Add(levelScene);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        //protected override void UnloadContent()
        //{
        //    // TODO: Unload any non ContentManager content here
        //}

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            base.Draw(gameTime);
        }


        // draw title. Only called when sprite is begun
        public void DrawTitle()
        {
            Vector2 tempPosition = positionTitle;
            tempPosition.X -= 5;
            tempPosition.Y -= 5;
            Sprite.Begin();
            // draw back title
            Sprite.DrawString(titleFont, titleText, tempPosition, titleColorBakground);
            // draw front title
            Sprite.DrawString(titleFont, titleText, positionTitle, titleColor);
            Sprite.End();
        }

        // draw background (general one). Only called when sprite is begun
        public void DrawGeneralBackground()
        {
            // draw background
            Sprite.Begin();
            Sprite.Draw(background, new Vector2(0, 0), Color.White);
            Sprite.End();
        }

        // set current scene based on user selection
        public void Notify(GameScene sender, string action)
        {
            // hide current scene first
            currentScene.hide();
            if(sender is MenuScene)
            {
                switch (action)
                {
                    case "Start Game":
                        currentScene = levelScene;
                        break;

                    case "Help":
                        currentScene = helpScene;
                        break;

                    case "High Score":
                        currentScene = scoreScene;
                        break;

                    case "About":
                        currentScene = creditScene;
                        break;

                    case "Quit":
                        Exit();
                        break;
                }
            }
            else if (sender is LevelScene)
            {
                switch (action)
                {
                    case "Easy":
                    case "Hard":
                        currentScene = actionScene;
                        actionScene.SetLevel(action == "Hard");
                        break;

                    case "Escape":
                        currentScene = menuScene;
                        break;
                }
            }
            // all the following menu will return to the same menu scene
            else if ((sender is ActionScene) || (sender is HelpScene) || (sender is ScoreScene) || (sender is CreditScene))
            {
                currentScene = menuScene;
            }

            currentScene.show();
        }

        public string GetLeaderBoardFilename(bool isDifficult)
        {
            if (isDifficult)
            {
                return leaderboardFilenameHard;
            }
            else
            {
                return leaderboardFilenameEasy;
            }
        }

    }
}
