﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalProject
{
    public class MenuScene : GameScene
    {

        // menu item properties
        private SpriteFont menuFont;
        // text color of not selected options
        private Color menuColor = Color.White;
        // text color of selected option
        private Color selectedMenuColor = Color.Yellow;
        // position to display menu
        private Vector2 positionMenuItem;

        // menu items
        private List<string> menuItems;
        // which item  is selected
        private int selectedIndex;

        // previous state of keyboard
        private KeyboardState oldState;

        public MenuScene(Game game, List<string> menuItems, SpriteFont menuFont) : base(game)
        {
            parent = (Game1)game;
            // load font
            this.menuFont = menuFont;

            // set menu start position
            positionMenuItem = new Vector2(parent.Stage.X / 2 - 100 , parent.Stage.Y / 2);

            // set menu items
            this.menuItems = menuItems;
        }


        public override void Draw(GameTime gameTime)
        {
            // draw background 
            parent.DrawGeneralBackground();
            // draw title
            parent.DrawTitle();

            // start drawing
            parent.Sprite.Begin();
            // temp position of menu item
            Vector2 tempPosition = positionMenuItem;

            // draw each menu items
            for(int i = 0; i < menuItems.Count; i++)
            {
                // if selected then display with different color
                if (selectedIndex == i)
                {
                    parent.Sprite.DrawString(menuFont, menuItems[i], tempPosition, selectedMenuColor);
                }
                else
                {
                    parent.Sprite.DrawString(menuFont, menuItems[i], tempPosition, menuColor);
                }
                // add spacing
                tempPosition.Y += menuFont.LineSpacing;
            }

            // end drawing
            parent.Sprite.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState state = Keyboard.GetState();
            // move one position up
            if (oldState.IsKeyUp(Keys.Down) && state.IsKeyDown(Keys.Down))
            {
                selectedIndex = MathHelper.Clamp(selectedIndex + 1, 0, menuItems.Count - 1);
            }
            // move one position down
            if (oldState.IsKeyUp(Keys.Up) && state.IsKeyDown(Keys.Up))
            {
                selectedIndex = MathHelper.Clamp(selectedIndex - 1, 0, menuItems.Count - 1);
            }
            // enter is hit, go to page based on user selected value
            if (oldState.IsKeyUp(Keys.Enter) && state.IsKeyDown(Keys.Enter))
            {
                parent.Notify(this, menuItems[selectedIndex]);
            }
            oldState = state;

            base.Update(gameTime);
        }

    }
}
