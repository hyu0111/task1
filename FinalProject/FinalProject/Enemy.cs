﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    public class Enemy : DrawableGameComponent
    {
        Game1 parent;

        // all 2d resources used for this enemy. There are possibly more than one photo to display if the enemy has more than 1 live
        Texture2D[] source;
        // which photo to display for the enemy
        int sourcePosition = 0;

        // speed the enemy travels
        Vector2 speed;
        // start point of the enemy
        Vector2 startPosition;

        // int LiveCount { get; set; }
        public Enemy(Game game, int level, Texture2D[] resources, Vector2 speed) : base(game)
        {
            // check if level is valid. Should one one of 0, 1, 2
            if (level <= -1 || level >= 3)
            {
                throw new Exception("Invalid enemy level");
            }
            parent = (Game1)game;
            // all resources the enemy uses
            source = resources;
            // travel speed of the enemy
            this.speed = speed;

            // randomly select the start point of the plane
            float startPositionY = (float) new Random().Next(0, parent.GAME_HEIGHT - source[0].Height);
            // the start position of enemy. Always start from the rightmost point of the stage (x axis)
            startPosition = new Vector2(parent.Stage.X, startPositionY);

            // LiveCount = source.Length;
        }

        public override void Draw(GameTime gameTime)
        {
            parent.Sprite.Begin();
            // draw the plane with the relevent resource
            parent.Sprite.Draw(source[sourcePosition], startPosition, Color.White);
            parent.Sprite.End();
            base.Draw(gameTime);
        }

        public bool OutOfScreen()
        {
            // if the enemy travels to the left most point of the screen
            // does not include those who has no live already
            return startPosition.X + source[0].Width <= 0;
        }

        public void Hit()
        {
            // go to next resource position
            // same as lose one live
            sourcePosition += 1;
            // LiveCount -= 1;
            // if (LiveCount == 0)
            if (sourcePosition == source.Length) // out of range
            {
                this.Visible = false;
                this.Enabled = false;
            }
        }

        public bool isAlive()
        {
            // return LiveCount > 0;
            // check if the enemy is still alive (has more resources to show)
            return ((sourcePosition < source.Length) && (this.Visible == true) && (this.Enabled == true));
        }

        public override void Update(GameTime gameTime)
        {
            if (isAlive())
            {
                // move left if it is still alive
                startPosition -= speed;
            }
            base.Update(gameTime);
        }

        public Rectangle GetHitArea()
        {
            // get the hit area of the enemy
            // do not want collcison happen at the emeny head, but the body, so + with / 3.
            return new Rectangle((int) startPosition.X +  source[0].Width / 3,
                                 (int)startPosition.Y,
                                 (int)startPosition.X + source[0].Width,
                                 (int)startPosition.Y + source[0].Height);
        }
    }
}
