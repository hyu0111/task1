﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    public class Plane : DrawableGameComponent
    {
        Game1 parent;
        // plane source image
        private Texture2D plane;
        // current position of the plane
        private Vector2 position;
        // travel speed of plane when moving
        private Vector2 speed = new Vector2(10, 10);

        // list of bullets shoot by the plane
        private List<Bullet> bullets;

        // previous state of keyboard
        private KeyboardState oldState;

        // Check if space is released
        private bool bulletCreated = true;

        public Plane(Game game) : base(game)
        {
            parent = (Game1) game;
            // load plane resource
            plane = parent.Content.Load<Texture2D>("Plane/Plane");
            // plane starts at middle left of stage
            position = new Vector2(0, parent.Stage.Y / 2);
            // create new empty list of bullets
            bullets = new List<Bullet>();
        }

        public override void Draw(GameTime gameTime)
        {
            parent.Sprite.Begin();
            // draw the plane
            parent.Sprite.Draw(plane, position, Color.White);
            parent.Sprite.End();
            foreach(Bullet b in bullets)
            {
                // draw each bullet
                b.Draw(gameTime);
            }
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState state = Keyboard.GetState();
            // update new position of airplane based on the button pressed
            if (state.IsKeyDown(Keys.Right))
            {
                position.X += speed.X;
            }
            if (state.IsKeyDown(Keys.Left))
            {
                position.X -= speed.X;
            }
            if (state.IsKeyDown(Keys.Up))
            {
                position.Y -= speed.Y;
            }
            if (state.IsKeyDown(Keys.Down))
            {
                position.Y += speed.Y;
            }
            // trim position so we don't go out of the screen
            position.X = MathHelper.Clamp(position.X, 0, parent.Stage.X - plane.Width);
            position.Y = MathHelper.Clamp(position.Y, 0, parent.Stage.Y - plane.Height);

            // create new bullet
            if (oldState.IsKeyUp(Keys.Space) && state.IsKeyDown(Keys.Space))
            {
                // we don't want a long bullet list when user holds the space, so we restrict to one bullet at each space holding
                if (!bulletCreated)
                {
                    Bullet newBullet = new Bullet(parent, new Vector2(position.X + plane.Width, position.Y + plane.Height / 2));
                    bullets.Add(newBullet);
                    // bullet is created for this space hold round already
                    bulletCreated = true;
                }
            }
            else
            {
                // space is not pressed, so we set bullet to not yet created
                bulletCreated = false;
            }

            // check each bullet
            for (int i = bullets.Count - 1; i >= 0; --i)
            {
                // if the bullet is out of the screen or collide with enemy already
                // then we remove the bullet from the list
                if (bullets[i].OutOfScreen() || bullets[i].IsHit())
                {
                    bullets.RemoveAt(i);
                }
                else
                {
                    // otherwise we update the bullet's position
                    bullets[i].Update(gameTime);
                }
            }
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        // get list of bullets
        public List<Bullet> GetBullets()
        {
            return bullets;
        }

    }
}
