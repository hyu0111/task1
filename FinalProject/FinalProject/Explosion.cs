﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    // explosion animation
    public class Explosion : DrawableGameComponent
    {
        private Game1 parent;
        // explosion resources
        private Texture2D explosion;
        // width and height of explosion
        private int width;
        private int height;

        // x and y axis of the frame currently displays
        private int currentX = 0;
        private int currentY = 0;

        // start position to display explosion
        private Vector2 position;

        // indicates if the animation is done
        // private bool animationDone = false;

        // sound effect of enemy aircraft being hit
        private SoundEffect hitSound;
        // if the sound is played. We only play it once
        bool soundPlayed = false;


        public Explosion(Game game, Vector2 position) : base(game)
        {
            parent = (Game1)game;
            // load explosion resources
            explosion = parent.Content.Load<Texture2D>("explosion/explosion");
            // there are 9 frames. so we divide it to 3 x 3 box
            // width and height of one frame
            width = explosion.Width / 3;
            height = explosion.Height / 3;

            // where to display the explosion. Expected to be the same as the head of bullet
            this.position = position;

            // hit sound effect
            hitSound = parent.Content.Load<SoundEffect>("sound/hit");
        }

        public override void Draw(GameTime gameTime)
        {
            if (!soundPlayed)
            {
                // play sound once if it is not done yet
                hitSound.Play();
                soundPlayed = true;
            }

            // rectangle to display the explosion source
            Rectangle source = new Rectangle(currentX, currentY, width, height); 
            parent.Sprite.Begin();
            // display the current frame resource
            parent.Sprite.Draw(explosion, position, source, Color.White);
            parent.Sprite.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            // go to next frame
            currentX += width;
            // if the current row is done go to next row
            if (currentX >= explosion.Width)
            {
                currentX = 0;
                currentY += height;
                // if we finish last row already then the animation is done
                if (currentY >= explosion.Height)
                {
                    this.Enabled = false;
                    this.Visible = false;
                    // animationDone = true;
                }
            }
            base.Update(gameTime);
        }

        // check if animation is done
        public bool AnimationDone()
        {
            return ((!this.Enabled) && (!this.Visible));
        }
    }
}
