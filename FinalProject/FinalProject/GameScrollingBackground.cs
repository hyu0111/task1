﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    // scrolling background used during game action scene
    public class GameScrollingBackground : DrawableGameComponent
    {
        Game1 parent;
        // scrolling background source
        Texture2D scrollingBackground;
        // scrolling speed
        Vector2 speed = new Vector2(1, 0);
        // start position
        Vector2 position1 = new Vector2(0, 0);
        // second image start position
        Vector2 position2;
        // rectangle that displays the source
        Rectangle rect;

        public GameScrollingBackground(Game game) : base(game)
        {
            parent = (Game1) game;
            // load the scrolling background resource
            scrollingBackground = parent.Content.Load<Texture2D>("background/background2");
            // set position of second image to one image width further
            position2 = new Vector2(position1.X + scrollingBackground.Width, position1.Y);
            // rectangle that will contain the background
            rect = new Rectangle(0, 0, parent.GAME_WIDTH, parent.GAME_HEIGHT);

        }

        public override void Draw(GameTime gameTime)
        {
            parent.Sprite.Begin();
            // draw the background with given position
            parent.Sprite.Draw(scrollingBackground, position1, rect, Color.White);
            parent.Sprite.Draw(scrollingBackground, position2, rect, Color.White);
            parent.Sprite.End();

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            // move both image to the left
            position1 -= speed;
            position2 -= speed;

            // if the first image is fully out of screen at the left, then move it to the right
            if (position1.X < -scrollingBackground.Width)
            {
                position1.X = position2.X + scrollingBackground.Width;
            }
            // if the second image is fully out of screen at the left, then move it to the right
            if (position2.X < -scrollingBackground.Width)
            {
                position2.X = position1.X + scrollingBackground.Width;
            }
            base.Update(gameTime);
        }
    }
}
