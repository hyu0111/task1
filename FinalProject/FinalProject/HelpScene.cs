﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    class HelpScene : GameScene
    {
        // position to write help text
        Vector2 positionHelpText;
        // font of help content
        SpriteFont helpFont;
        // color of help texts
        Color helpColor = Color.White;

        // help text to return to menu
        private string returnToMain = "Press ESC to return to main menu";

        // lines of help texts
        private string[] helpItems = { 
            "You need to shoot the enemy aircrafts and",
            "  don't let them lit the left line", 
            "UP/DOWN/LEFT/RIGHT: aircraft control",
            "SPACE: shooting",
            "ENTER: select menu options/resume game",
            "ESC: pause/ back to memu"
        };

        // previous state of keyboard
        private KeyboardState oldState;

        public HelpScene(Game game, SpriteFont helpFont) : base(game)
        {
            // position to start writing help texts
            positionHelpText = new Vector2(parent.Stage.X / 8, parent.Stage.Y / 2);
            this.helpFont = helpFont;
        }

        public override void Draw(GameTime gameTime)
        {
            // draw background 
            parent.DrawGeneralBackground();
            // draw title
            parent.DrawTitle();

            // start drawing
            parent.Sprite.Begin();
            // temp position of help texts
            Vector2 tempPosition = positionHelpText;

            // draw each help items
            for (int i = 0; i < helpItems.Length; i++)
            {
                parent.Sprite.DrawString(helpFont, helpItems[i], tempPosition, helpColor);
                // move one line down
                tempPosition.Y += helpFont.LineSpacing;
            }
            // additional spacing
            tempPosition.Y += helpFont.LineSpacing;
            // help text for returning to menu
            parent.Sprite.DrawString(helpFont, returnToMain, tempPosition, helpColor);

            // end drawing
            parent.Sprite.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState state = Keyboard.GetState();

            // tell parent to return to menu if ESC is pressed
            if (oldState.IsKeyUp(Keys.Escape) && state.IsKeyDown(Keys.Escape))
            {
                parent.Notify(this, "escape");
            }
            oldState = state;
            base.Update(gameTime);
        }
    }
}
