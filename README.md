## Installing the project

You can install the application with the following step:

1. Click "Clone" on the main page of this project and copy the line
2. On command prompt, paste the line and enter
3. Go to task1/FinalProject/FinalProject/publish, double clickn *setup.exe*
4. Follow the setup instruction
5. The program will launch automatically

---
## Reason of Using MIT License
The reason of choosing MIT license is this project can be re-distributed for any use. However, the project is not originally created to use other than assignment (academic project), the owner does not want to be responsible for any issues comes with the project, such as bugs. MIT license states the project is not warrant and liable for any issue of the software. 